from PIL import Image, ImageDraw
import qrcode

FLAG = input('flag: ')

img = qrcode.make(FLAG, box_size=1, border=0)

data = img.load()

vert = [list(map(len, filter(None, ''.join([str(int(data[x, y] == 0)) for y in range(img.height)]).split('0')))) or '0' for x in range(img.width)]
horz = [list(map(len, filter(None, ''.join([str(int(data[x, y] == 0)) for x in range(img.width)]).split('0')))) or '0' for y in range(img.height)]

print('{"ver":'+repr(horz)+',"hor":'+repr(vert)+'}')

indentX = max([len(i)*2-1 for i in vert])*9
indentY = max([len(i)*2-1 for i in horz])*9

step_size = 16

height = img.height*step_size + indentY
width = img.width*step_size + indentX
image = Image.new(mode='L', size=(width, height), color=255)

draw = ImageDraw.Draw(image)
y_start = indentY
y_end = image.height

for i in range(img.width):
    y = i*step_size + indentY
    text = ' '.join(map(str, horz[i]))
    w, h = draw.textsize(text)
    draw.text((indentX-w-8, y+step_size/2-h/2), text)

for x in range(indentX, image.width, step_size):
    line = ((x, y_start), (x, y_end))
    draw.line(line, fill=128)

x_start = indentX
x_end = image.width

for i in range(img.height):
    x = i*step_size + indentX
    text = '\n'.join(map(str, vert[i]))
    w, h = draw.textsize(text)
    draw.text((x+step_size/2-w/2, indentY-h-8), text)

for y in range(indentY, image.height, step_size):
    line = ((x_start, y), (x_end, y))
    draw.line(line, fill=128)

image.save('out.png')
